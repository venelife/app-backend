"""
Encounters app models
"""
from django.db import models
from django.contrib.postgres.fields import DateTimeRangeField
from django.utils.translation import ugettext_lazy as _

from core.models import ValueSet
from conditions.models import Condition
from profiles.models import PatientProfile, PractitionerProfile
from referrals.models import ReferralRequest
from organizations.models import Location


class EncounterStatus(ValueSet):
    """Current state of the encounter.

    Defining URL:	http://hl7.org/fhir/ValueSet/encounter-status
    """
    pass


class ActEncounterCode(ValueSet):
    """The kind of Act of the Encounter.

    Defining URL:	http://hl7.org/fhir/ValueSet/v3-ActEncounterCode
    """
    pass


class EncounterPriority(ValueSet):
    """Urgency under which the Act happened, can happen, is happening, is intended
    to happen, or is requested/demanded to happen.

    Defining URL:   http://hl7.org/fhir/ValueSet/v3-ActPriority
    """
    pass


class DiagnosisRole(ValueSet):
    """Role of a diagnosis on the Encounter

    Defining URL:	http://hl7.org/fhir/ValueSet/diagnosis-role
    """
    pass


class Diagnosis(models.Model):
    """List of diagnosis relevant to the encounter
    """
    condition = models.ForeignKey(Condition, on_delete=models.CASCADE)
    role = models.ForeignKey(DiagnosisRole, on_delete=models.CASCADE)


class ParticipantType(ValueSet):
    """Set of codes that can be used to indicate how an individual participates in
    an encounter.

    Defining URL:   http://hl7.org/fhir/ValueSet/encounter-participant-type
    """
    pass


class EncounterParticipant(models.Model):
    """List of participants involved in the encounter.
    """
    encounter = models.ForeignKey('Encounter', on_delete=models.CASCADE)
    participant = models.ForeignKey(PractitionerProfile, on_delete=models.CASCADE)
    participant_type = models.ForeignKey(
        ParticipantType, on_delete=models.CASCADE,
        verbose_name=_('cómo participa')
    )
    period = DateTimeRangeField(verbose_name='periodo de participación')


class EncounterReason(ValueSet):
    """Encounter reason codes.

    Defining URL:	http://hl7.org/fhir/ValueSet/encounter-reason
    """
    pass


class Encounter(models.Model):
    """An interaction during which services are provided to the patient

    Defining URL:	https://www.hl7.org/fhir/encounter.html
    """
    status = models.ForeignKey(
        EncounterStatus, on_delete=models.CASCADE,
        verbose_name=_('estado del encuentro')
    )
    encounter_class = models.ForeignKey(
        ActEncounterCode, on_delete=models.CASCADE,
        verbose_name=_('acción del encuentro')
    )
    priority = models.ForeignKey(
        EncounterPriority, on_delete=models.CASCADE,
        verbose_name=_('prioridad del encuentro')
    )
    subject = models.ForeignKey(
        PatientProfile, on_delete=models.CASCADE,
        verbose_name=_('paciente sujeto del encuentro')
    )
    participant = models.ManyToManyField(
        PractitionerProfile, through=EncounterParticipant)
    appointment = models.ForeignKey(
        'appointments.Appointment', on_delete=models.CASCADE,
        verbose_name=_('La cita que programó este encuentro')
    )
    incoming_referral = models.ManyToManyField(ReferralRequest)
    period = DateTimeRangeField(verbose_name='periodo del encuentro')
    reason = models.ForeignKey(
        EncounterReason, on_delete=models.CASCADE,
        verbose_name=_('razón del encuentro')
    )
    diagnosis = models.ManyToManyField(Diagnosis)
    location = models.ManyToManyField(Location)
