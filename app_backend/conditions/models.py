"""
Conditions app models
"""
from django.db import models
from django.utils.translation import ugettext_lazy as _

from core.models import ValueSet
from profiles.models import PatientProfile, PractitionerProfile


class ConditionClinicalStatus(ValueSet):
    """Condition Clinical Status Codes

    Defining URL:	http://hl7.org/fhir/ValueSet/condition-clinical
    """
    pass


class ConditionVerificationStatus(ValueSet):
    """The verification status to support or decline the clinical status of the
    condition or diagnosis.

    Defining URL:   http://hl7.org/fhir/ValueSet/condition-ver-status
    """
    pass


class ConditionSeverity(ValueSet):
    """Condition/Diagnosis Severity

    Defining URL:	http://hl7.org/fhir/ValueSet/condition-severity
    """
    pass


class ConditionCode(ValueSet):
    """Condition/Problem/Diagnosis Codes

    Defining URL:   http://hl7.org/fhir/ValueSet/condition-code This value set

    Includes content from SNOMED CT
    """
    pass


class BodySite(ValueSet):
    """SNOMED CT Body Structures

    Defining URL:	http://hl7.org/fhir/ValueSet/body-site

    Includes content from SNOMED CT
    """
    pass


class ManifestationSympton(ValueSet):
    """Manifestación o síntoma

    Defining URL:	http://hl7.org/fhir/ValueSet/manifestation-or-symptom
    """
    pass


class Condition(models.Model):
    """	Detailed information about conditions, problems or diagnoses
    """
    clinical_status = models.ForeignKey(
        ConditionClinicalStatus, on_delete=models.CASCADE,
        verbose_name=_('estado clínico')
    )
    verification_status = models.ForeignKey(
        ConditionVerificationStatus, on_delete=models.CASCADE,
        verbose_name=_('estado de la verificación')
    )
    severity = models.ForeignKey(
        ConditionSeverity, on_delete=models.CASCADE,
        verbose_name=_('severidad subjetiva')
    )
    code = models.ForeignKey(
        ConditionCode, on_delete=models.CASCADE,
        verbose_name=_('identificación de la condición')
    )
    body_site = models.ForeignKey(
        BodySite, on_delete=models.CASCADE,
        verbose_name=_('ubicación anatómica')
    )
    subject = models.ForeignKey(
        PatientProfile, on_delete=models.CASCADE,
        verbose_name=_('paciente sujeto de la condición')
    )
    context = models.ForeignKey(
        'encounters.Encounter', on_delete=models.CASCADE,
        verbose_name=_('encuentro de la primera constatación')
    )
    onset = models.DateField(verbose_name=_('tiempo estimado de inicio'))
    abatement = models.DateField(verbose_name=_('fecha estimada de remisión'))
    asserted = models.DateField(verbose_name=_('fecha de verificación'))
    asserter = models.ForeignKey(
        PractitionerProfile, on_delete=models.CASCADE,
        verbose_name=_('verificador')
    )
    evidence = models.ManyToManyField(
        ManifestationSympton,
        verbose_name=_('evidencia de respaldo')
    )
