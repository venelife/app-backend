"""
Referrals app models
"""
from django.db import models
from django.utils.translation import ugettext_lazy as _

from core.models import ValueSet
from profiles.models import PatientProfile, PractitionerProfile


class RequestStatus(ValueSet):
    """Codes identifying the stage lifecycle stage of a request

    Defining URL:	http://hl7.org/fhir/ValueSet/request-status
    """

    class Meta:
        verbose_name_plural = _('Request status')


class ReferralType(ValueSet):
    """This value set includes all SNOMED CT Patient Referral.

    Defining URL:	http://hl7.org/fhir/ValueSet/referral-type
    """
    pass


class RequestPriority(ValueSet):
    """The clinical priority of a diagnostic order.

    Defining URL:	http://hl7.org/fhir/ValueSet/request-priority
    """
    pass


class ClinicalFindings(ValueSet):
    """SNOMED CT Clinical Findings

    Defining URL:	http://hl7.org/fhir/ValueSet/clinical-findings
    """

    class Meta:
        verbose_name_plural = "clinical findings"


class ReferralRequest(models.Model):
    """A request for referral or transfer of care

    Defining URL:	http://hl7.org/fhir/referralrequest.html
    """
    status = models.ForeignKey(
        RequestStatus, on_delete=models.CASCADE,
        verbose_name=_('estado de la remisión')
    )
    referral_type = models.ForeignKey(
        ReferralType, on_delete=models.CASCADE,
        verbose_name=_('tipo de remisión')
    )
    priority = models.ForeignKey(
        RequestPriority, on_delete=models.CASCADE,
        verbose_name=_('prioridad de la solicitud')
    )
    subject = models.ForeignKey(
        PatientProfile, on_delete=models.CASCADE,
        verbose_name=_('paciente sujeto de la remisión')
    )
    context = models.ForeignKey(
        "encounters.Encounter", on_delete=models.CASCADE,
        verbose_name=_('encuentro de origen')
    )
    authored_on = models.DateTimeField(
        verbose_name=_('Fecha y hora de la creación'),
        auto_now=True
    )
    requester = models.ForeignKey(
        PractitionerProfile, on_delete=models.CASCADE,
        verbose_name=_('profesional que solicita el servicio')
    )
    reason = models.ForeignKey(
        ClinicalFindings, on_delete=models.CASCADE,
        verbose_name=_('razón de la remisión')
    )
    description = models.TextField(
        verbose_name=_('descripción textual de la remisión')
    )
