"""
Appointments app models
"""
from django.db import models
from django.contrib.postgres.fields import DateTimeRangeField
from django.utils.translation import ugettext_lazy as _

from core.models import ValueSet
from profiles.models import Profile
from encounters.models import ParticipantType, EncounterReason
from conditions.models import Condition
from referrals.models import ReferralRequest


class AppointmentStatus(ValueSet):
    """The free/busy status of an appointment.

    Defining URL:	http://hl7.org/fhir/ValueSet/appointmentstatus
    """
    pass


class ServiceCategory(ValueSet):
    """	Codes that can be used to classify groupings of service-types/specialties.

    Defining URL:	http://hl7.org/fhir/ValueSet/service-category
    """
    pass


class ServiceType(ValueSet):
    """Codes of service-types.

    Defining URL:	http://hl7.org/fhir/ValueSet/service-type
    """
    pass


class PracticeCode(ValueSet):
    """Clinical specialty of the clinician or provider.

    Defining URL:	http://hl7.org/fhir/ValueSet/c80-practice-codes
    """
    pass


class AppointmentReason(ValueSet):
    """Appointment reason codes

    Value Set URL:	http://hl7.org/fhir/ValueSet/v2-0276
    """
    pass


class Schedule(models.Model):
    """Container for slots of time that may be available for booking appointments.
    """
    active = models.BooleanField(verbose_name=_('activo'))
    service_category = models.ForeignKey(ServiceCategory, on_delete=models.CASCADE)
    service_type = models.ForeignKey(ServiceType, on_delete=models.CASCADE)
    specialty = models.ForeignKey(PracticeCode, on_delete=models.CASCADE)
    actor = models.ForeignKey(Profile, on_delete=models.CASCADE)
    planning_horizon = DateTimeRangeField(verbose_name='vigencia de los intervalos')


class SlotStatus(ValueSet):
    """The free/busy status of the slot.

    Defining URL:	http://hl7.org/fhir/ValueSet/slotstatus
    """
    pass


class Slot(models.Model):
    """A slot of time on a schedule that may be available for booking appointments.
    """
    service_category = models.ForeignKey(ServiceCategory, on_delete=models.CASCADE)
    service_type = models.ForeignKey(ServiceType, on_delete=models.CASCADE)
    specialty = models.ForeignKey(PracticeCode, on_delete=models.CASCADE)
    appointment_type = models.ForeignKey(AppointmentReason, on_delete=models.CASCADE)
    schedule = models.ForeignKey(Schedule, on_delete=models.CASCADE)
    status = models.ForeignKey(SlotStatus, on_delete=models.CASCADE)
    period = DateTimeRangeField(verbose_name='vigencia de los intervalos')
    overbooked =  models.BooleanField(verbose_name=_('capacidad excedida'))


class ParticipantRequired(ValueSet):
    """Is the Participant required to attend the appointment.

    Defining URL:	http://hl7.org/fhir/ValueSet/participantrequired
    """
    pass


class ParticipationStatus(ValueSet):
    """The Participation status of an appointment.

    Defining URL:	http://hl7.org/fhir/ValueSet/participationstatus
    """
    pass


class AppointmentParticipant(models.Model):
    """Participants involved in appointment
    """
    participant_type = models.ForeignKey(ParticipantType, on_delete=models.CASCADE)
    actor = models.ForeignKey(Profile, on_delete=models.CASCADE)
    required = models.ForeignKey(ParticipantRequired, on_delete=models.CASCADE)
    status = models.ForeignKey(ParticipationStatus, on_delete=models.CASCADE)
    appointment = models.ForeignKey('Appointment', on_delete=models.CASCADE)


class Appointment(models.Model):
    """Cita de un evento de atención médica entre paciente(s), médico(s),
    persona(s) relacionada(s) y/o dispositivo(s) para una fecha/hora específica.
    Esto puede resultar en uno o más Encuentros
    """
    status = models.ForeignKey(AppointmentStatus, on_delete=models.CASCADE)
    service_category = models.ForeignKey(ServiceCategory, on_delete=models.CASCADE)
    service_type = models.ForeignKey(ServiceType, on_delete=models.CASCADE)
    specialty = models.ForeignKey(PracticeCode, on_delete=models.CASCADE)
    appointment_type = models.ForeignKey(AppointmentReason, on_delete=models.CASCADE)
    reason = models.ForeignKey(EncounterReason, on_delete=models.CASCADE)
    indication = models.ForeignKey(Condition, on_delete=models.CASCADE)
    priority = models.PositiveSmallIntegerField(verbose_name='prioridad')
    description = models.TextField(verbose_name='descripción')
    #  supporting_information
    period = DateTimeRangeField(verbose_name='vigencia de los intervalos')
    slot = models.ForeignKey(Slot, on_delete=models.CASCADE)
    created = models.DateTimeField(verbose_name=_('fecha de creación'), auto_created=True)
    commment = models.TextField(verbose_name='descripción')
    incoming_referral = models.ForeignKey(ReferralRequest, on_delete=models.CASCADE)
    participant =  models.ManyToManyField(Profile, through=AppointmentParticipant)