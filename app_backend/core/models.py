from django.db import models
from django.utils.translation import ugettext_lazy as _


class ValueSet(models.Model):
    """A set of codes drawn from one or more code systems

    Defining URL:	hl7.org/fhir/valueset.html
    """
    code = models.CharField(_('código'), max_length=50)
    display = models.CharField(_('etiqueta'), max_length=100, default='')

    def __str__(self):
        return self.code

    class Meta:
        abstract = True



