"""
Profiles app models
"""
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from core.models import ValueSet


class Gender(ValueSet):
    """The gender of a person used for administrative purposes.

    Defining URL:	http://hl7.org/fhir/ValueSet/administrative-gender
    """
    pass


class ContactPointSystem(ValueSet):
    """Telecommunications form for contact point.

    Defining URL:	http://hl7.org/fhir/ValueSet/contact-point-system
    """
    pass


class ContactPointUse(ValueSet):
    """Use of contact point.

    Defining URL:	http://hl7.org/fhir/ValueSet/contact-point-use
    """
    pass


class ContactPoint(models.Model):
    """Details of a Technology mediated contact point (phone, fax, email, etc.)

    Defining URL:	https://www.hl7.org/fhir/datatypes.html#contactpoint
    """
    system = models.ForeignKey(
        ContactPointSystem, on_delete=models.CASCADE,
        verbose_name=_('sistema de punto de contacto')
    )
    use = models.ForeignKey(
        ContactPointUse, on_delete=models.CASCADE,
        verbose_name=_('uso del punto de contacto')
    )
    value = models.CharField(_('valor'), max_length=20)

    def __str__(self):
        return self.value


class IdentifierType(ValueSet):
    """	Identifier Type Codes

    Defining URL:	http://hl7.org/fhir/ValueSet/identifier-type
    """
    pass


class Identifier(models.Model):
    """An identifier intended for computation

    Defining URL:	https://www.hl7.org/fhir/datatypes.html#identifier
    """
    identifier_type = models.ForeignKey(
        IdentifierType, on_delete=models.CASCADE,
        verbose_name=_('tipo de identificador')
    )
    value = models.CharField(_('valor'), max_length=20)

    def __str__(self):
        return self.value


class Profile(models.Model):
    """User profile in system, corresponds to a Person Resource

    Defining URL:	https://www.hl7.org/fhir/person.html
    """
    user = models.OneToOneField(
        User, on_delete=models.CASCADE,
        verbose_name=_('usuario')
    )
    telecom = models.ForeignKey(
        ContactPoint, on_delete=models.CASCADE,
        verbose_name=_('punto de contacto')
    )
    gender = models.ForeignKey(
        Gender, on_delete=models.CASCADE,
        verbose_name=_('género')
    )
    identifier = models.ForeignKey(
        Identifier,
        on_delete=models.CASCADE,
        verbose_name=_('identificador')
    )
    birthdate = models.DateField(_('fecha de nacimiento'))
    address = models.CharField(_('dirección'), max_length=100)


class PatientProfile(models.Model):
    """Patient profile in system, corresponds to a Patient Resource

    Defining URL:	https://www.hl7.org/fhir/patient.html
    """
    profile = models.OneToOneField(
        Profile, on_delete=models.CASCADE,
        verbose_name=_('perfil')
    )
    deceased = models.BooleanField(
        _('fallecido'),
        help_text='Indicates if the individual is deceased or not.'
    )
    deceased_time = models.DateField(_('tiempo de fallecido'))


class QualificationCode(ValueSet):
    """Coded representation of the qualification
    FHIR Value set/code system definition for HL7 v2 table 0360
    ver 2.8.2 (Degree/License/Certificate)

    Defining URL:   https://www.hl7.org/fhir/v2/0360/2.7/index.html
    """
    pass


class Qualification(models.Model):
    """Practitioner's qualifications obtained by training and certification

    """
    identifier = models.CharField(_('identificador'), max_length=100)
    qualification_code = models.ForeignKey(
        QualificationCode, on_delete=models.CASCADE,
        verbose_name=_('código de la titulación')
    )
    models.CharField(_('código'), max_length=100)


class PractitionerProfile(models.Model):
    """Practitioner profile in system, corresponds to a Practitioner Resource
    A person who is directly or indirectly involved in the provisioning of
    healthcare. 

    Defining URL:   https://www.hl7.org/fhir/practitioner.html
    """
    qualification = models.ForeignKey(
        Qualification, on_delete=models.CASCADE,
        verbose_name=_('calificación')
    )
    profile = models.OneToOneField(
        Profile, on_delete=models.CASCADE,
        verbose_name=_('perfil')
    )
