"""
Organizations app models
"""
from django.db import models
from core.models import ValueSet
from django.utils.translation import ugettext_lazy as _

from profiles.models import ContactPoint


class OrganizationType(ValueSet):
    """Codes that can be used to indicate a type of organization.

    Defining URL:	http://hl7.org/fhir/ValueSet/organization-type
    """
    pass


class ContacEntityType(ValueSet):
    """Codes that can be used to indicate the purpose for which you would contact a
    contact party.

    Defining URL:   http://hl7.org/fhir/ValueSet/contactentity-type
    """
    pass


class Contact(models.Model):
    """Contact for the organization for a certain purpose.
    """
    purpose = models.ForeignKey(ContacEntityType, on_delete=models.CASCADE)
    name = models.CharField(_('nombre'), max_length=181)
    telecom = models.ForeignKey(ContactPoint, on_delete=models.CASCADE)
    address = models.CharField(_('dirección'), max_length=100)


class LocationStatus(ValueSet):
    """Indicates whether the location is still in use.

    Defining URL:	http://hl7.org/fhir/ValueSet/location-status
    """
    pass


class LocationMode(ValueSet):
    """Indicates whether a resource instance represents a specific location or a class of locations.

    Defining URL:	http://hl7.org/fhir/ValueSet/location-mode
    """
    pass


class LocationRoleType(ValueSet):
    """A role of a place that further classifies the setting (e.g., accident site,
    road side, work site, community location) in which services are delivered.

    Defining URL:
    http://hl7.org/fhir/ValueSet/v3-ServiceDeliveryLocationRoleType
    """
    pass


class LocationPhysicalType(ValueSet):
    """ This example value set defines a set of codes that can be used to indicate
    the physical form of the Location.

    Defining URL:   http://hl7.org/fhir/ValueSet/location-physical-type
    """
    pass


class Location(models.Model):
    """
    Detalles e información del lugar físico donde se prestan servicios y los
    recursos y participantes pueden ser almacenados, encontrados, contenidos o
    acomodados.
    """
    status = models.ForeignKey(LocationStatus, on_delete=models.CASCADE)
    name = models.CharField(_('nombre'), max_length=100)
    alias = models.CharField(_('alias'), max_length=100)
    description = models.TextField(verbose_name='descripción')
    mode = models.ForeignKey(LocationMode, on_delete=models.CASCADE)
    role_type = models.ForeignKey(LocationRoleType, on_delete=models.CASCADE)
    telecom = models.ManyToManyField(ContactPoint)
    physical_type = models.ForeignKey(LocationPhysicalType, on_delete=models.CASCADE)
    longitude = models.DecimalField(
        verbose_name='longitud',
        decimal_places=5, max_digits=9
    )
    latitude = models.DecimalField(
        verbose_name='latitud',
        decimal_places=5, max_digits=9
    )
    altitude = models.DecimalField(
        verbose_name='altitud',
        decimal_places=3, max_digits=9
    )


class Organization(models.Model):
    """A grouping of people or organizations with a common purpose.
    """
    active = models.BooleanField(verbose_name=_('activo'))
    organization_type = models.ForeignKey(OrganizationType, on_delete=models.CASCADE)
    name = models.CharField(_('nombre'), max_length=100)
    alias = models.CharField(_('alias'), max_length=100)
    telecom = models.ManyToManyField(ContactPoint)
    address = models.CharField(_('dirección'), max_length=100)
    part_of = models.ForeignKey('Organization', on_delete=models.CASCADE,)
    contact = models.ManyToManyField(Contact)
    location = models.ForeignKey(Location, on_delete=models.CASCADE, blank=True, null=True)
